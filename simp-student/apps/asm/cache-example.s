# cache-example.s
    .set noreorder
    .data
    .align 16   # align to 2^16 address boundary (16 LSBs all 0)
A:  .space 1024

    .text
    .globl main
    .ent main
main:
        lui     $t0, %hi(A)
        ori     $t0, $t0, %lo(A)

        sw      $t1, 48($t0)
        sw      $t1, 80($t0)
        lw      $t1, 48($t0)
        lw      $t1, 84($t0)

        ori     $v0, $0, 10     # exit
        syscall
    .end main
