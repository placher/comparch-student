# slt-hazard-complete.s
    .set noreorder
    .data

    .text
    .globl main
    .ent main
main:
# x = 1
        addi    $t0, $0, 1      # x = 1
# y = 2
        addi    $t1, $0, 2      # y = 2
# if_ (!(x < y))
        slt     $t2, $t0, $t1   # t2 = x < y
        bne     $t2, $0, L      # if (t2) goto L
        nop
#   w = 0xaa
        ori     $t3, $0, 0xaa   # w = 0xaa
L:
# z = 0xbb
        ori     $t4, $0, 0xbb   # z = 0xbb
        ori     $v0, $0, 10     # exit
        nop
        nop
        nop
        syscall
    .end main
