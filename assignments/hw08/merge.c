#include "mipslib.h"
#define SIZE 1000

int a[SIZE];
int b[SIZE];
int m_w = 1, m_z = 2;

int randInt() {
    m_z = 36969 * (m_z & 65535) + (m_z >> 16);
    m_w = 18000 * (m_w & 65535) + (m_w >> 16);
    return (m_z << 16) + m_w;
}

void merging(int low, int mid, int high){
    int l1, l2, i;
    
    for(l1 = low, l2 = mid+1, i = low; l1 <= mid && l2 <= high; i++){
        if(a[l1] <= a[l2])
            b[i] = a[l1++];
        else
            b[i] = a[l2++];
    }
    while(l1 <= mid)
        b[i++] = a[l1++];
    while(l2 <= high)
        b[i++] = a[l2++];
    for(i = low; i<=high;i++)
        a[i] = b[i];
}

void sort(int low, int high){
    int mid;
    
    if(low < high){
        mid = (low + high) / 2;
        sort(low, mid);
        sort(mid+1, high);
        merging(low, mid, high);
    }else{
        return;
    }
}

int main() {
    int i;
    
    for(i = 0; i < SIZE; i++){
        a[i] = randInt();
        //printf("%d ", a[i]);
    }
    sort(0, SIZE);
    /*for(i = 0; i < SIZE; i++){
        printf("%d ", a[i]);
    }*/
    return 0;
}
