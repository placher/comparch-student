import csv
import os
import numpy as np
import matplotlib.pyplot as plt

#associativity
associativities = np.array([1, 2, 4, 8])

#block size
block = 4 #log_2(16 bytes/block)

#capacity
num_words = [32, 64, 128, 256, 512, 1024, 2048, 4096]
log_2_num_words = [5, 6, 7, 8, 9, 10, 11, 12]

plt.hold(True)
for words in num_words:
	run = []
	for associativity in associativities:
		#calculate sets-per-way
		sets = int(np.log2((words/(associativity*block))))
		
		#run isimp
		cmd = ('isimp -dcache '+str(associativity)+' '+str(sets)+' '+str(block)+' a.out')
		print cmd
		os.system(cmd)
		
		#get miss rate data
		stats_dict = dict(csv.reader(open('stats.csv')))
		run.append(float(stats_dict['dCacheMissRate']))
	plt.plot(associativities,run,'-o')
plt.xlabel('Associativity')
plt.ylabel('Miss Rate')
plt.legend(num_words)
plt.title('Miss Rate vs. Associativity and Capacity (in words)')
plt.savefig('part2_miss_rate_vs_associativity.png')
plt.hold(False)
