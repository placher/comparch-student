import csv
import os
import numpy as np
import matplotlib.pyplot as plt

#direct-mapped
associativity = 1

#block size
log_2_block_size = np.array([2, 3, 4, 5, 6, 7]) #log_2(bytes-per-block)
block_size = np.array([1, 2, 4, 8, 16, 32])

#capacity
num_words = [32, 64, 128, 256, 512, 1024, 2048, 4096]
log_2_num_words = [5, 6, 7, 8, 9, 10, 11, 12]

plt.hold(True)
for words in num_words:
	run = []
	for block in log_2_block_size:
		#calculate sets per way
		blk = int((2**block)/4)		
		sets = int(np.log2(words/blk))

		#run isimp
		cmd = ('isimp -dcache '+str(associativity)+' '+str(sets)+' '+str(block)+' a.out')
		print cmd
		os.system(cmd)
		
		#get miss rate data
		stats_dict = dict(csv.reader(open('stats.csv')))
		run.append(float(stats_dict['dCacheMissRate']))
	plt.plot(block_size,run,'-o')
plt.xlabel('Block Size (words)')
plt.ylabel('Miss Rate')
plt.xscale('log', basex=2)
plt.legend(num_words)
plt.title('Miss Rate vs. Block Size and Capacity (in words)')
plt.savefig('part2_miss_rate_vs_block_size.png')
plt.hold(False)
