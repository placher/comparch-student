#homework 3b problem 2
#J. Patrick Lacher
	.set noreorder
	.data

	.text
	.globl main
	
	.ent print
print:
	addi $t0,$sp,-52
	lw $a0,($t0)
	ori $v0,$zero,20
	syscall
	jr $ra
	nop
	.end print

	.ent sum3
sum3:
	addi $t3,$sp,-48
	lw $t0,($t3)
	lw $t1,4($t3)
	lw $t2,8($t3)
	add $t0,$t0,$t1
	add $t0,$t0,$t2
	sw $t0,12($t3)
	jr $ra 
	nop
	.end sum3

	.ent polynomial
polynomial:
	#int x;
	#int y;
	#int z;
		addi $t7,$sp,-32
		sw $ra,24($t7)
		lw $t3,($t7)
		lw $t4,4($t7)
		lw $t5,8($t7)
		lw $t6,12($t7)
	#x = a << b;
		sll $t0,$t3,$t4
	#y = c << d;
		sll $t1,$t5,$t6
	#z = sum3(x, y, e);
		addi $t7,$sp,-48
		sw $t0,($t7)
		sw $t1,4($t7)
		lw $t3,32($t7)
		sw $t3,8($t7)
		jal sum3
		nop
	#print(x);
		addi $t7,$sp,-52
		lw $t0,4($t7)
		sw $t0,($t7)
		jal print
		nop
	#print(y);
		addi $t7,$sp,-52
		lw $t0,8($t7)
		sw $t0,($t7)
		jal print
		nop
	#print(z);
	#return(z);
		addi $t7,$sp,-52
		lw $t0,16($t7)
		sw $t0,($t7)
		sw $t0,40($t7)
		jal print
		nop
		addi $t7,$sp,-8
		lw $ra,($t7)
		jr $ra
		nop
	.end polynomial

	.ent main
main:
		addi $t0,$sp,-4
		sw $ra,($t0)
	#int a = 2;
		li $t1,2
 	#int f = polynomial(a, 3, 4, 5, 6);
		addi $t0,$sp,-32
		sw $t1,($t0)
		li $t1,3
		sw $t1,4($t0)
		li $t1,4
		sw $t1,8($t0)
		li $t1,5
		sw $t1,12($t0)
		li $t1,6
		sw $t1,16($t0)
		jal polynomial
		nop
 	#print(a);
		addi $t0,$sp,-52
		lw $t1,20($t0)
		sw $t1,($t0)
		jal print
		nop
 	#print(f);
		addi $t0,$sp,-52
		lw $t1,40($t0)
		sw $t1,($t0)
		jal print
		nop
		addi $t0,$sp,-4
		lw $ra,($t0)
		jr $ra
		nop
	.end main
