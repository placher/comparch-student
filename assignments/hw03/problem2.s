#homework 3 problem 2
#J. Patrick Lacher
	.set noreorder
	.data
	
	#char A[] = { 1, 25, 7, 9, -1 };
	A: .byte 1, 25, 7, 9, -1
	
	.text
	.globl main
	.ent main
main:
	#int i;
 		add $s0,$zero,$zero
	#int current;
		add $s1,$zero,$zero
	#int max;
		add $s2,$zero,$zero
	#i = 0;
		add $s0,$zero,$zero
	#current = A[0];
		la $t0, A
		lb $s1,($t0)
	#max = 0;
		add $s2,$zero,$zero
	#while (current > 0) {
		loop:
		slt $t1,$zero,$s1
		beq $t1,0,exit
		nop
	#if (current > max)
		slt $t1,$s2,$s1
		beq $t1,0,skip
		nop
	#max = current;
		add $s2,$s1,$zero
	#i = i + 1;
		skip:
		addi $s0,$s0,1
	#current = A[i];
		add $t2,$t0,$s0
		lb $s1,($t2)
		j loop
	#}
		exit:
	#PRINT_HEX_DEC(max);
		add $a0,$zero,$s2
		ori $v0,$zero,20
		syscall
	#EXIT;
		ori $v0,$zero,10
		syscall
	.end main
