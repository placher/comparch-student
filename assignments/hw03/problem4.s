#homework 3 problem 4
#J. Patrick Lacher
	.set noreorder
	.data
	#typedef struct record {
	#int field0;
	#int field1;
	#} record;
		record: .space 8
	.text
	.globl main
	.ent main
main:
	#record *r = (record *) MALLOC(sizeof(record));
		li $a0,8
		ori $v0,$zero,9
		syscall
	#r->field0 = 100;
		li $t0,100
		sw $t0,($v0)
	#r->field1 = -1;
		li $t0,-1
		sw $t0,4($v0)
	#EXIT;
		ori $v0,$zero,10
		syscall
	.end main
