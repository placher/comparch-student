#homework 03 problem 3
#J. Patrick Lacher
	.set noreorder
	.data

	#int A[8]	
	A: .space 32

	.text
	.globl main
	.ent main
main:
	#int i;
		add $s0,$zero,$zero
	#A[0] = 0;
		la $t5,A
		sw $zero,($t5)
	#A[1] = 1;
		addi $t1,$zero,1
		addi $t4,$t5,4
		sw $t1,($t4)
	#for (i = 2; i < 8; i++) {
		addi $s0,$zero,2
		loop:
		slti $t1,$s0,8
		beq $t1,$zero,exit
		nop
	#A[i] = A[i-1] + A[i-2]
		addi $t3, $t4, 4 #A(i)
		lw $t6,($t4)
		lw $t7,($t5)
		add $t7,$t6,$t7
		sw $t7,($t3)
		addi $t4,$t4,4
		addi $t5,$t5,4
	#PRINT_HEX_DEC(A[i]);
		lw $a0,($t3)
		ori $v0,$zero,20
		syscall
		addi $s0,$s0,1
		j loop
		nop
	#}
		exit:
	#EXIT;
		ori $v0,$zero,10
		syscall
	.end main
