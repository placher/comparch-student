#homework 3 problem 5
#J. Patrick Lacher
	.set noreorder
	.data

	#typedef struct elt {
	#int value;
	#struct elt *next;
	#} elt;
	elt: .space 8

	.text
	.globl main
	.ent main
main:
	#elt *head;
		#$s0
	#elt *newelt;
		#$s1
	#newelt = (elt *) MALLOC(sizeof (elt));
		li $a0,8
		ori $v0,$zero,9
		syscall
		add $s1,$v0,$zero
	#newelt->value = 1;
		li $t0,1
		sw $t0,($s1)
	#newelt->next = 0;
		sw $zero,4($s1)
	#head = newelt;
		add $s0,$s1,$zero
	#newelt = (elt *) MALLOC(sizeof (elt));
		li $a0,8
		ori $v0,$zero,9
		syscall
		add $s1,$v0,$zero
	#newelt->value = 2;
		li $t0,2
		sw $t0,($s1)
	#newelt->next = head;
		sw $s0,4($s1)
	#head = newelt;
		add $s0,$s1,$zero
	#PRINT_HEX_DEC(head->value);
		lw $a0,($s0)
		ori $v0,$zero,20
		syscall
	#PRINT_HEX_DEC(head->next->value);
		lw $t0,4($s0)
		lw $a0,($t0)
		ori $v0,$zero,20
		syscall
	#EXIT;
		ori $v0,$zero,10
		syscall
	.end main
