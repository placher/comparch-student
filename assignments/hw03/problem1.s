#homework 3 problem1
#J. Patrick Lacher

	.set noreorder
	.data

	.text
	.globl main
	.ent main
main:
	#int score = 84;
		addi $t0,$zero,84
		
	#int grade;
		add $s0,$zero,$zero
	
	#if (score >= 90)
		addi $t1,$zero,89
		slt $t2,$t1,$t0
	
	#grade = 4;
		addi $s0,$zero,4
		beq $t2,1,end
	
	#else if (score >= 80)
		addi $t1,$zero,79
		slt $t2,$t1,$t0
 
	#grade = 3;
		addi $s0,$zero,3
		beq $t2,1,end
	
	#else if (score >= 70)
		addi $t1,$zero,69
		slt $t2,$t1,$t0

	#grade = 2;
		addi $s0,$zero,2
		beq $t2,1,end

	#else
	
	#grade = 0;
		add $s0,$zero,$zero	

	#PRINT_HEX_DEC(grade);
		end:
		add $a0,$zero,$s0
		ori $v0, $zero, 20
		syscall
	#EXIT;
		ori $v0,$zero,10
		syscall
	.end main

