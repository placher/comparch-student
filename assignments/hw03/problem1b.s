#homework 3b problem 1
	.set noreorder
	.data

	.text
	.globl main
	.ent print
print:
	#void print(int a)
	#{
		ori $v0,$zero,20
		syscall
		jr $ra
		nop
	#}
	.end print
	.ent main
main:
	#int A[8];
		addi $s0,$sp,-36
		sw $ra,32($s0)
	#int i;
		addi $s2,$s0,-4
	#A[0] = 0;
		sw $zero,($s0)
	#A[1] = 1;
		li $t0,1
		addi $s1,$s0,4
		sw $t0,($s1)
	#for (i = 2; i < 8; i++) {
		addi $s2,$zero,2
		loop:
		slti $t0,$s2,8
		beq $t0,$zero,exit
		nop
	#A[i] = A[i-1] + A[i-2];
		addi $s3,$s1,4
		lw $t0,($s0)
		lw $t1,($s1)
		add $t2,$t0,$t1
		sw $t2,($s3)
		addi $s0,$s0,4
		addi $s1,$s1,4
	#print(A[i]);
		add $a0,$zero,$t2
		jal print
		nop
		addi $s2,$s2,1
		j loop
		nop
	#}
	exit:
	addi $s0,$sp,-4
	lw $ra,($s0)
	jr $ra
	nop
.end main
