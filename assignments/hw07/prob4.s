#cache-example.s
	.set noreorder
	.data
	.align 16
A:	.space 1024
	.text
	.globl main
	.ent main
main:
		lui	$t0, %hi(A)
		ori	$t0, $t0, %lo(A)
	
		sw	$t1, 0($t0)
		sw	$t1, 4($t0)
		sw	$t1, 8($t0)
		sw	$t1, 64($t0)
		sw	$t1, 68($t0)
		sw	$t1, 72($t0)
		sw	$t1, 128($t0)
		sw	$t1, 132($t0)
		sw	$t1, 136($t0)
		lw	$t1, 0($t0)
		lw	$t1, 4($t0)
		lw	$t1, 8($t0)
		lw	$t1, 64($t0)
		lw	$t1, 68($t0)
		lw	$t1, 72($t0)
		lw	$t1, 128($t0)
		lw	$t1, 132($t0)
		lw	$t1, 136($t0)

		ori $v0, $0, 10
		syscall
	.end main
